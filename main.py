from TaskManager import TaskManager

class InputManager:
    def __init__(self, inputs = []):
        self.inputs = inputs
        self.iter = iter(self.inputs)

    def getInput(self):
        if (len(self.inputs) == 0):
            return input(">>> ")
        else:
            return next(self.iter)

class Main:
    def __init__(self):
        self.taskManager = TaskManager()
        self.InputManager = InputManager()

    def GetUserInput(self):
        return self.InputManager.getInput()
        
    def ParseUserInput(self, input):
        firstSpace = input.find(" ")

        action = input[:firstSpace]
        argument = input[firstSpace+1:]

        if (action == "+"):
            self.taskManager.AddTask(argument)
        elif (action == "x"):
            self.taskManager.MarkDoneTask(int(argument))
        elif (action == "o"):
            self.taskManager.MarkToDoTask(int(argument))

    def DisplayTasks(self):
        if (len(self.taskManager.tasks) == 0):
            print("No task yet")
        else:
            for task in self.taskManager.tasks:
                print("{} {} {}".format(task.id, task.status, task.description))

    def MainLoop(self):
        keepLooping = True
        self.DisplayTasks()

        while (keepLooping):
            keepLooping = self.Loop()

    def Loop(self):
        userInput = self.GetUserInput()

        if (userInput == "q"):
            print("Bye!")
            return False
        else:
            self.ParseUserInput(userInput)
            self.DisplayTasks()
            return True