from Task import Task
from TaskManager import TaskManager
import main as script

def test_CreateMain():
    main = script.Main()

    assert isinstance(main.taskManager, TaskManager)
    assert(main.taskManager.currentId == 1)
    assert(main.taskManager.tasks == [])

def test_GetUserInput(monkeypatch):
    main = script.Main()
    expected = "1"

    monkeypatch.setattr("builtins.input", lambda x: expected)
    actual = main.GetUserInput()
    assert actual == expected

def test_ParseUserInput_AddTask():
    main = script.Main()

    main.ParseUserInput("+ Test")

    assert len(main.taskManager.tasks) == 1
    assert main.taskManager.tasks[0].id == 1
    assert main.taskManager.tasks[0].description == "Test"
    assert main.taskManager.tasks[0].status == "[ ]"

def test_ParseUserInput_MarkTaskDone():
    main = script.Main()

    task = Task(1, "Test", "[ ]")
    main.taskManager.tasks =  [task]

    main.ParseUserInput("x 1")

    assert len(main.taskManager.tasks) == 1
    assert main.taskManager.tasks[0].id == 1
    assert main.taskManager.tasks[0].description == "Test"
    assert main.taskManager.tasks[0].status == "[x]"

def test_ParseUserInput_MarkTaskToDo():
    main = script.Main()

    task = Task(1, "Test", "[ ]")
    main.taskManager.tasks =  [task]

    main.ParseUserInput("o 1")

    assert len(main.taskManager.tasks) == 1
    assert main.taskManager.tasks[0].id == 1
    assert main.taskManager.tasks[0].description == "Test"
    assert main.taskManager.tasks[0].status == "[o]"

def test_DisplayTasks(capsys):
    main = script.Main()
    
    task = Task(1, "Test", "[ ]")
    main.taskManager.tasks = [task]

    main.DisplayTasks()
    captured = capsys.readouterr()

    assert captured.out == "1 [ ] Test\n"
    assert captured.err == ""

def test_DisplayTasks_NoTask(capsys):
    main = script.Main()

    main.DisplayTasks()
    captured = capsys.readouterr()

    assert captured.out == "No task yet\n"
    assert captured.err == ""

def test_MainLoop_Exit(monkeypatch, capsys):
    main = script.Main()

    monkeypatch.setattr("builtins.input", lambda x: "q")
    main.MainLoop()

    captured = capsys.readouterr()

    assert captured.out == "No task yet\nBye!\n"
    assert captured.err == ""

def test_Loop_Exit(monkeypatch, capsys):
    main = script.Main()

    monkeypatch.setattr("builtins.input", lambda x: "q")
    returnValue = main.Loop()

    captured = capsys.readouterr()

    assert captured.out == "Bye!\n"
    assert captured.err == ""
    assert returnValue == False

def test_Loop_AddTask(monkeypatch, capsys):
    main = script.Main()

    monkeypatch.setattr("builtins.input", lambda x: "+ Learn Python")
    returnValue = main.Loop()

    captured = capsys.readouterr()

    assert captured.out == "1 [ ] Learn Python\n"
    assert captured.err == ""
    assert returnValue == True

def test_Loop_MarkDoneTask(monkeypatch, capsys):
    main = script.Main()
    task = Task(1, "Learn Python", "[ ]")
    main.taskManager.tasks = [task]

    monkeypatch.setattr("builtins.input", lambda x: "x 1")
    returnValue = main.Loop()

    captured = capsys.readouterr()

    assert captured.out == "1 [x] Learn Python\n"
    assert captured.err == ""
    assert returnValue == True

def test_Loop_MarkToDoTask(monkeypatch, capsys):
    main = script.Main()
    task = Task(1, "Learn Python", "[ ]")
    main.taskManager.tasks = [task]

    monkeypatch.setattr("builtins.input", lambda x: "o 1")
    returnValue = main.Loop()

    captured = capsys.readouterr()

    assert captured.out == "1 [o] Learn Python\n"
    assert captured.err == ""
    assert returnValue == True

def test_InputManager():
    inputManager = script.InputManager(["+ Learn Python", "+ Learn Java", "q"])

    a = inputManager.getInput()
    b = inputManager.getInput()
    c = inputManager.getInput()

    assert a == "+ Learn Python"
    assert b == "+ Learn Java"
    assert c == "q"

def test_MainLoop_AddTask(capsys):
    main = script.Main()
    inputs = ["+ Learn Python", "q"]
    main.InputManager.inputs = inputs
    main.InputManager.iter = iter(inputs)

    main.MainLoop()

    captured = capsys.readouterr()

    assert captured.out == "No task yet\n1 [ ] Learn Python\nBye!\n"
    assert captured.err == ""

def test_MainLoop_MarkDoneTask(capsys):
    main = script.Main()
    task = Task(1, "Learn Python", "[ ]")
    main.taskManager.tasks = [task]
    inputs = ["x 1", "q"]
    main.InputManager.inputs = inputs
    main.InputManager.iter = iter(inputs)

    main.MainLoop()

    captured = capsys.readouterr()

    assert captured.out == "1 [ ] Learn Python\n1 [x] Learn Python\nBye!\n"
    assert captured.err == ""

def test_MainLoop_MarkToDoTask(capsys):
    main = script.Main()
    task = Task(1, "Learn Python", "[ ]")
    main.taskManager.tasks = [task]
    inputs = ["o 1", "q"]
    main.InputManager.inputs = inputs
    main.InputManager.iter = iter(inputs)

    main.MainLoop()

    captured = capsys.readouterr()

    assert captured.out == "1 [ ] Learn Python\n1 [o] Learn Python\nBye!\n"
    assert captured.err == ""