from Task import Task

class TaskManager:
    def __init__(self):
        self.tasks = []
        self.currentId = 1

    def AddTask(self, description):
        task = Task(self.currentId, description, "[ ]")
        self.tasks.append(task)
        self.currentId += 1
        
    def RemoveTask(self, id):
        self.tasks = [task for task in self.tasks if task.id != id]

    def MarkTest(self, id, mark):
        for task in self.tasks:
            if task.id == id:
                task.status = mark

    def MarkDoneTask(self, id):
        self.MarkTest(id, "[x]")

    def MarkToDoTask(self, id):
        self.MarkTest(id, "[o]")