import pytest
import TaskManager as script
from Task import Task

def test_CreateTaskManager():
    taskManager = script.TaskManager()

    assert isinstance(taskManager, script.TaskManager)
    assert taskManager.tasks == []
    assert taskManager.currentId == 1

def test_CreateTask():
    task = Task(1, "description", "status")

    assert task.id == 1
    assert task.description == "description"
    assert task.status == "status"

def test_AddTask():
    taskManager = script.TaskManager()

    taskManager.AddTask("Test")

    assert len(taskManager.tasks) == 1
    assert taskManager.tasks[0].id == 1
    assert taskManager.tasks[0].description == "Test"
    assert taskManager.tasks[0].status == "[ ]"
    assert taskManager.currentId == 2

def test_RemoveTask():
    taskManager = script.TaskManager()
    task = Task(1, "Test", "[ ]")
    taskManager.tasks = [task]

    taskManager.RemoveTask(1)

    assert taskManager.tasks == []

def test_MarkDoneTask():
    taskManager = script.TaskManager()
    task = Task(1, "Test", "[ ]")
    taskManager.tasks = [task]

    taskManager.MarkDoneTask(1)

    assert len(taskManager.tasks) == 1
    assert taskManager.tasks[0].id == 1
    assert taskManager.tasks[0].description == "Test"
    assert taskManager.tasks[0].status == "[x]"

def test_MarkToDoTask():
    taskManager = script.TaskManager()
    task = Task(1, "Test", "[ ]")
    taskManager.tasks = [task]

    taskManager.MarkToDoTask(1)

    assert len(taskManager.tasks) == 1
    assert taskManager.tasks[0].id == 1
    assert taskManager.tasks[0].description == "Test"
    assert taskManager.tasks[0].status == "[o]"

def test_MarkTest():
    taskManager = script.TaskManager()
    task = Task(1, "Test", "[ ]")
    taskManager.tasks = [task]

    taskManager.MarkTest(1, "x")

    assert len(taskManager.tasks) == 1
    assert taskManager.tasks[0].id == 1
    assert taskManager.tasks[0].description == "Test"
    assert taskManager.tasks[0].status == "x"